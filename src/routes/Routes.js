import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";

import {Types} from "../pages/types/Types";
import {Container} from "../components/Container";
import {StringExamples} from "../pages/string/StringExamples";
import {Menu} from "../components/Menu";
import {NumberExamples} from "../pages/number/NumberExamples";
import {Scope} from "../pages/scope/Scope";
import {Objects} from "../pages/objects/Objects";
import {Promises} from "../pages/async/Promises";
import {Fetch} from "../pages/async/Fetch";
import {Variables} from "../pages/types/Variables";
import {Intro} from "../pages/intro/Intro";
import {ObjectExamples} from "../pages/objects/ObjectExamples";
import {ArrayExamples} from "../pages/objects/ArrayExamples";
import {String} from "../pages/string/String";
import {Array} from "../pages/objects/Array";
import {Closure} from "../pages/scope/Closure";
import {Functions} from "../pages/functions/Functions";
import {FunctionExamples} from "../pages/functions/FunctionExamples";
import {Prototypes} from "../pages/objects/Prototypes";
import {This} from "../pages/objects/This";
import {Classes} from "../pages/classes/Classes";
import {ErrorHandling} from "../pages/misc/ErrorHandling";
import {Conditions} from "../pages/misc/Conditions";
import {Modules} from "../pages/misc/Modules";

export function Routes() {
    return (
        <Router>
            <div className="container-custom">
                <Menu/>
                <Container>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/intro"/>
                        </Route>

                        <Route path="/intro">
                            <Intro/>
                        </Route>

                        <Route path="/types">
                            <Types/>
                        </Route>

                        <Route path="/variables">
                            <Variables/>
                        </Route>

                        <Route path="/object-examples">
                            <ObjectExamples/>
                        </Route>

                        <Route path="/array">
                            <Array/>
                        </Route>

                        <Route path="/array-examples">
                            <ArrayExamples/>
                        </Route>

                        <Route path="/number-examples">
                            <NumberExamples/>
                        </Route>

                        <Route path="/string">
                            <String/>
                        </Route>

                        <Route path="/string-examples">
                            <StringExamples/>
                        </Route>

                        <Route path="/conditions">
                            <Conditions/>
                        </Route>

                        <Route path="/objects">
                            <Objects/>
                        </Route>

                        <Route path="/scope">
                            <Scope/>
                        </Route>

                        <Route path="/promises">
                            <Promises/>
                        </Route>

                        <Route path="/closures">
                            <Closure/>
                        </Route>

                        <Route path="/functions">
                            <Functions/>
                        </Route>

                        <Route path="/functions-examples">
                            <FunctionExamples/>
                        </Route>

                        <Route path="/fetch">
                            <Fetch/>
                        </Route>

                        <Route path="/prototypes">
                            <Prototypes/>
                        </Route>

                        <Route path="/this">
                            <This/>
                        </Route>

                        <Route path="/classes">
                            <Classes/>
                        </Route>

                        <Route path="/errors">
                            <ErrorHandling/>
                        </Route>

                        <Route path="/modules">
                            <Modules/>
                        </Route>

                        <Route path="/promises">
                            <Promises/>
                        </Route>
                    </Switch>
                </Container>
            </div>
        </Router>
    );
}
