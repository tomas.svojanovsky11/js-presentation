export const menuItems = [
    {
        label: "Intro",
        link: "/intro",
    },
    {
        label: "Typy",
        link: "/types",
    },
    {
        label: "Proměnné",
        link: "/variables",
    },
    {
        label: "String",
        link: "/string",
    },
    {
        label: "String použití",
        link: "/string-examples",
    },
    {
      label: "Podmínky",
      link: "/conditions",
    },
    {
        label: "Pole",
        link: "/array",
    },
    {
        label: "Pole použití",
        link: "/array-examples",
    },
    {
        label: "Objekty",
        link: "/objects",
    },
    {
        label: "Objekt použití",
        link: "/object-examples",
    },
    {
        label: "Funkce",
        link: "/functions",
    },
    {
        label: "Funkce použití",
        link: "/functions-examples",
    },
    {
      label: "Moduly",
      link: "/modules",
    },
    {
        label: "Scope",
        link: "/scope",
    },
    {
        label: "Closures",
        link: "/closures",
    },
    {
        label: "This",
        link: "/this",
    },
    {
        label: "Prototypy",
        link: "/prototypes",
    },
    {
        label: "Třídy",
        link: "/classes",
    },
    {
        label: "Errory",
        link: "/errors",
    },
    {
        label: "Fetch",
        link: "/fetch",
    },
];
