import styled from "styled-components";

export const MenuContainer = styled.nav`
  background-color: #457b9d;
  height: 100vh;
  position: sticky;
  top: 0;
  overflow: scroll;
  
  .active {
    background-color: #f1faee;
    color: #111111;
  }
  
  ol {
    padding-left: 0;
    padding: 2em;
    
    > li {
      color: #fff;
      
      + li {
        margin-top: 1em;
      }
      
      > a {
        display: block;
        color: #fff;
        text-decoration: none;
        padding: 0.5em 1em;
      }
    }
  }
`
