import React from 'react';

export function Content({ children, title }) {
    return (
        <div>
            <h1>{title}</h1>
            {children}
        </div>
    );
}
