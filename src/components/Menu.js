import React from "react";
import {NavLink} from "react-router-dom";

import {MenuContainer} from "./Menu.styled";
import {menuItems} from "./menuItems";

export function Menu() {
    return (
        <MenuContainer>
            <ol>
                {menuItems.map(({link, label}) => (
                    <li key={link}>
                        <NavLink to={link}>{label}</NavLink>
                    </li>
                ))}
            </ol>
        </MenuContainer>
    );
}
