import styled from "styled-components";

export const ContainerStyled = styled.div`
  display: flex;
  padding: 1em;
  background-color: #257ca3;
  color: #fff;
  min-height: 100vh;
  width: 100%;
  
  > div {
    width: 100%;
  }
`
