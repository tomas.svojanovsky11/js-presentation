import React from 'react';
import {Code} from "../../components/Code";

export function Intro() {
    return (
        <div>
            <h1>Intro</h1>

            <p>Javascript se často plete s Javou. Na počátku se jmenoval Javascript Mocha. Interně v netscape se jmenoval Livescript.</p>
            <p>Oficiálně vyšel pod jménem javascript. Bohužel ochrannou známku vlastní Oracla (přes Netscape).</p>

        <a href="https://www.ecma-international.org/ecma-262/12.0/">Specifikace</a>
        <a href="https://tc39.es/process-document/">Process document</a>
        <a href="https://github.com/tc39/proposals">Proposals</a>

            <h3>Web browser</h3>

            <h3>Node</h3>

            <p>Nodejs je prostředí, které umožňuje spouštět javascript mimo webový prohlížeč. Je postavený na V8 engine.</p>

            <h3>NPM</h3>

            <p>Balíčkovací systém</p>
            <p>Balíčky jsou uloženy v složce node_modules</p>

            <h3>Paradigm</h3>

            <p>Procedurální</p>
            <p>OOP</p>
            <p>Funkcionální</p>

           <h3>Kompilace kódu</h3>

          <p>1. Tokenizace</p>

          <Code>
              {`const name = "Tomas";
const, a, 2 a ;`}
          </Code>

          <p>2. Parsing</p>
          <p>AST - Abstract syntax tree</p>
          <p>const - VariableDeclaration</p>
          <p>name - Identifier</p>
          <p>= - AssignmentExpression</p>
          <p>Tomas - StringLiteral</p>

          <p>3. Code generation</p>

         <Code>
                 {`let greeting = "Hello";

console.log(greeting);

greeting = ."Hi";
// SyntaxError: unexpected token .`}
         </Code>

          <Code>
                {`console.log("Howdy");

saySomething("Hello","Hi");
// Uncaught SyntaxError: Duplicate parameter name not
// allowed in this context

function saySomething(greeting,greeting) {
    "use strict";
    console.log(greeting);
}`}
          </Code>

        <Code>
                {`function saySomething() {
        let greeting = "Hello";
        {
        greeting = "Howdy";  // error comes from here
        let greeting = "Hi";
        console.log(greeting);
        }
}

saySomething();
// ReferenceError: Cannot access 'greeting' before
// initialization`}
        </Code>
        </div>
    );
}
