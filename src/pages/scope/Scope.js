import React from 'react';
import {Code} from "../../components/Code";

export function Scope() {
    return (
        <div>
            <h1>Scope</h1>

            <p>Scope: Kam se má js engine dívat na kódu</p>

            <Code>
                {`let teacher = "Tomas";
function otherClass() {
    teacher = "Pavel";
    const topic = "react";
    console.log("welcome!");
}

otherClass();
                
teacher;
topic;                `}
            </Code>
        </div>
    );
}
