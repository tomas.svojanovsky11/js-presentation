import React from 'react';
import {Code} from "../../components/Code";

export function Closure() {
    return (
        <div>
            <h1>Closure</h1>

            <p>Closure je taková funkce, která si pamatuje vnější proměnné, i když zavoláte funkci odkudkoliv.</p>

            <Code>
                {`function ask(question) {
    setTimeout(() => {
        console.log(question);
    }, 100);  
}

ask("What is closure?");`}
            </Code>

            <Code>
                {`function ask(question) {
    return function holdYourQuestion() {
        console.log(question);
    };
}

const myQuestion = ask("What is closure?");
myQuestion();`}
            </Code>
        </div>
    );
}
