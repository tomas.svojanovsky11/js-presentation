import React from 'react';
import {Code} from "../../components/Code";

export function ObjectExamples() {
    return (
        <div>
            <h1>Object examples</h1>

            <h3>Object.keys</h3>

            <Code>
                {`const product = {
    name: "5 years fixed",
    apr: 5,
    totalScore: 20000,              
};

Object.keys(product);`}
            </Code>

            <h3>Object.values</h3>


            <Code>
               {`const product = {
    name: "5 years fixed",
    apr: 5,
    totalScore: 20000,              
};

Object.values(product);`}
            </Code>
            <h3>Object.entries</h3>


            <Code>
                s{`const product = {
    name: "5 years fixed",
    apr: 5,
    totalScore: 20000,              
};

Object.entries(product);`}
            </Code>
        </div>
    );
}
