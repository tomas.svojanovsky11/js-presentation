import React from 'react';
import {Code} from "../../components/Code";

export function ArrayExamples() {
    return (
        <div>
            <h1>Array</h1>

        <h3>Push</h3>

        <Code>
        {`const colors = ["yellow', "green", "white"];
colors.push("black");`}
        </Code>

            <h3>Concat</h3>

        <Code>
                {`const colors = ["yellow', "green", "white"];
colors.concat(["black", "purple"]);`}
        </Code>

            <h3>Shift</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.shift();`}
                </Code>

            <h3>Unshift</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.unshift("black", "purple");`}
                </Code>

            <h3>Sort</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.sort();`}
                </Code>

            <h3>Pop</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.pop();`}
                </Code>

            <h3>Includes</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.includes("yellow");`}
                </Code>

            <h3>Map</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.map((color, index) => {
        return color + index;
});`}
                </Code>

            <h3>ForEach</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.forEach(color => {
        console.log(color);
});
                `}
                </Code>

            <h3>Filter</h3>

                <Code>
                        {`const colors = ["yellow', "green", "white"];
colors.filter(color => color.includes("y"));
                `}
                </Code>

            <h3>Reduce</h3>

        <Code>
                {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.reduce((prev, curr) => prev + curr,0);
                `}
        </Code>

            <h3>Find</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.find(val === 5);
                `}
                </Code>

            <h3>Slice</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.slice(2);
                `}
                </Code>

                <h3>Splice</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.slice(2);
                `}
                </Code>

            <h3>IndexOf</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.indexOf(2);
                `}
                </Code>

            <h3>Find index</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.findIndex(val => val === 2);
                `}
                </Code>

            <h3>Some</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.some(val => val < -1);
                `}
                </Code>
                
            <h3>Every</h3>

                <Code>
                        {`const numbers = [1, 2, 3, 4, 5, 6, 7];
numbers.every(val => val > 0);
                `}
                </Code>
        </div>
    );
}
