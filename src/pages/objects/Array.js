import React from 'react';
import {Code} from "../../components/Code";

export function Array() {
    return (
        <div>
            <h1>Pole</h1>

            <p>Pole je podtyp objektu</p>
            <p>V poli můžou být primitives i objekty</p>

            <Code>
                {`const names = ["Tomas", "Petr", "Pavel"];
                
names[0]; // Tomas

names[6] // undefined

delete names[0];

names.length;

const [first, ...others] = names;

names[6] = "Jan";
                `}
            </Code>
        </div>
    );
}
