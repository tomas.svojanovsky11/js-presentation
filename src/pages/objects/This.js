import React from 'react';
import {Code} from "../../components/Code";

export function This() {
    return (
        <div>
            <h1>This</h1>

            <p>Ve funkci this představuje "execution context", který se vytváří ve chvíli kdy je funkce zavoláná.</p>

            <Code>
                {`const workshop = {
    teacher: "Tomas",
    ask(question) {
        console.log(this.teacher, question);
    }                
}
workshop.ask("What is implicit binding?");`}
            </Code>


            <Code>
                {`function ask(question) {} {
    console.log(this.teacher, question);                
}

function otherClass() {
    const myContext = {
        teacher: "Jan",
    };
    ask.call(myContext);
}

otherClass();`}
            </Code>
        </div>
    );
}
