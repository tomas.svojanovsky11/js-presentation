import React from 'react';
import {Code} from "../../components/Code";

export function Objects() {
    return (
        <div>
            <h1>Objects</h1>

            <p>Neseřazená kolekce klíč - hodnota.</p>

            <Code>
                ${`const me = {
    name: "Tomas",
    age: 30,
    courses: [
        {   
            name: "Javascript",
        },
    ],
    colors: ["blue", "yellow"],
    answer() {
        console.log("Answering...");
    },            
};`}
            </Code>
        </div>
    );
}
