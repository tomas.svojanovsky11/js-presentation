import React from 'react';
import {Code} from "../../components/Code";

export function Prototypes() {
    return (
        <div>
            <h1>Prototypy</h1>

            <Code>
                {`function Workshop(teacher) {
    this.teacher = teacher;                
}

Workshop.prototype.ask = function(question) {
    console.log(this.teacher, question);
}

const javascript = new Workshop("Tomas");
const react = new Workshop("Jan");

javascript.ask("What is javascript");
react.ask("What is react?");`}
            </Code>
        </div>
    );
}
