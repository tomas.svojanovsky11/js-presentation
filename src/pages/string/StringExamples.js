import React from 'react';

import {Code} from "../../components/Code";

export function StringExamples() {
    return (
        <div>
            <h1>String examples</h1>

            <h3>Replace</h3>

            <Code>
                {`const date = "08 / 01 / 1995";
date.replace(" / ", "-"); // 08-01 / 1995

date.replace(/ \\/ /, "-"); // 08-01 / 1995
date.replace(/ \\/ /g, "-"); // 08-01-1995`}
            </Code>

            <h3>Substring</h3>

            <Code>
                {`const personalNumber = "11111-1";
personalNumber.substring(0, personalNumber.length - 2); // 11111`}
            </Code>

            <h3>Includes</h3>
            <Code>
                {`const code = "10123";
if (code.includes("23")) {
 console.log("Passed");
};`}
            </Code>

            <h3>toLowerCase x toUpperCase</h3>

            <Code>
                {`const name = "Vaclav";
name.toUpperCase(); // VACLAV
name.toLowerCase(); // vaclav`}
            </Code>

            <h3>Trim</h3>
        <Code>
                {`const city = " Prague ";
city.trim(); // Prague`}
        </Code>

            <h3>Split x Join</h3>

            <Code>
                {`const date = "08 / 01 / 1995";
date.split(" / ").join("-"); // 08-01-1995`}
            </Code>

            <h3>Slice</h3>
            <Code>
                {`const value = "101234545";
value.slice(-2); // 45`}
            </Code>

            <h3>Char at</h3>
            <Code>
                {`const city = "Prague";
city.charAt(0); // P`}
            </Code>
        </div>
    );
}
