import React from 'react';
import {Code} from "../../components/Code";

export function String() {
    return (
        <div>
            <h1>String</h1>

            <p>Běžně se můžete setkat s myšlenkou, že string je pole znaků.</p>

            <Code>
                {`const a = "Tomas";
const b ["T", "o", "m", "a", "s"];`}
            </Code>

            <Code>
                {`a.length;
b.length;

a.indexOf("m");
b.indexOf("m");`}
            </Code>

            <Code>
                {` a[1] = "O"; b[1] = "O";`}
            </Code>
        </div>
    );
}
