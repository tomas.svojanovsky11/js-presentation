import React from 'react';

import {Content} from "../../components/Content";
import {Code} from "../../components/Code";

export function Types() {


    return (
        <Content title="Typy">
            <p>V javascriptu je všechno objekt - false</p>

            <a href="https://tc39.es/ecma262/#sec-ecmascript-language-types" target="_blank">Ecma spec types</a>

            <Code>
                {`undefined          undeclared
string             null
number             function
boolean            array
object             bigint
symbol`}
            </Code>

            <h3>Primitives</h3>

            <Code>
                {`undefined
null
number
string
symbol
bigint`}
            </Code>

            <h3>Objects</h3>

            <Code>
                {`
object
function
array
                    `}
            </Code>

            <h3>NaN</h3>

            <p><strike>Not a number</strike></p>
            <p>Invalid number</p>

                <Code>
                        {`Number("xxx") // NaN `}
                </Code>

            <h3>Typeof</h3>

            <Code>
                {`typeof something
const x = 4;
typeof x; // "number"`}
            </Code>
        </Content>
    );
}
