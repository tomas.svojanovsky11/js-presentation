import React from 'react';
import {Code} from "../../components/Code";

export function Variables() {
    return (
        <div>
            <h1>Variables</h1>

            <Code>
                {`var name = "Tomas";
const name = "Tomas";
let name = "Tomas";`}
        </Code>
        </div>
    );
}
