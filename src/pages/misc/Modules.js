import React from 'react';
import {Code} from "../../components/Code";

export function Modules() {
    return (
        <div>
            <h1>Moduly</h1>

            <h3>Common js</h3>

            <Code>
                {`function getName() {
    console.log("my name is....");
}

function getLocation() {
    console.log("my location is...");
}

exports {
    getName,
    getLocation,
}`}
            </Code>

            <Code>
                {`function getName() {
    console.log("my name is....");
}

function getLocation() {
    console.log("my location is...");
}

module.exports.getName = getName;
module.exports.getLocation = getLocation;`}
            </Code>

            <h3>ES6 modules</h3>

            <p>Named export</p>

            <Code>
                {`import { Module } from "./SomeModule";`}
            </Code>

            <p>Default export</p>

            <Code>
                {`import Module from "./SomeModule";`}
            </Code>
        </div>
    );
}
