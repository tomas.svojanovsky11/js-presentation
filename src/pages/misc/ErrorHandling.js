import React from 'react';
import {Code} from "../../components/Code";

export function ErrorHandling() {
    return (
        <div>
            <h1>Error handling</h1>

            <Code>
                {`try {
} catch (e) {

} finally {
}`}
            </Code>

            <Code>
                {`class ValidationError extends Error {
                    constructor(message) {
                        super(message);
                    }
                }
                
new ValidationError("Age must be larger than 18")`}
            </Code>

            <Code>
                {`
                    try {
                     // nejaka logika
                     new ValidationError("Age must be larger than 18")
                    } catch (e) {
                        if (e instanceof ValidationError) {
                            // Display errors
                        } else {
                            // logout
                        }
                    }
                `}
            </Code>
        </div>
    );
}
