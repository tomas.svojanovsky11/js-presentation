import React from 'react';
import {Code} from "../../components/Code";

export function Conditions() {
    return (
        <div>
            <h1>Podmínky</h1>

            <Code>
                {`const age = 15;
                
if (age > 18) {
    console.log("Passed, let's go");
} else if (age > 65) {
    console.log("You are too old, consider it");
} else {
    console.log("You are too young, go away");
}`}
            </Code>


            <Code>
                {`const name = "Tomas";
switch (name) {
    case "Tomas":
     console.log("Hey, it's you again?");
     break;
    case "Jan":
        console.log("wait");
        break;
    default:
    console.log("I don't know you.");
}`}
            </Code>
        </div>
    );
}
