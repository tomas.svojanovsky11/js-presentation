import React from 'react';
import {Code} from "../../components/Code";

export function Classes() {
    return (
        <div>
            <h1>Classes</h1>

            <Code>
                {`class Workshop {
    constructor(teacher) {
        this.teacher = teacher;
    }
    
    ask(question) {
        console.log(this.teacher, question);
    }
}

const javascript = new Workshop("Tomas");
const react = new Workshop("Jan");

javascript.ask("What is javascript");
react.ask("What is react?");`}
            </Code>
        </div>
    );
}
