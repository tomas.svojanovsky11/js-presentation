import React from 'react';
import {Code} from "../../components/Code";

export function Fetch() {
    return (
        <div>
            <h1>Fetch</h1>

            <Code>
                ${`fetch('http://example.com/movies.json')
  .then(response => response.json())
  .then(data => console.log(data));`}
            </Code>
        </div>
    );
}
