import React from 'react';
import {Code} from "../../components/Code";

export function Promises() {
    return (
        <div>
            <h1>Promises</h1>

            <Code>
                {`const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('foo');
  }, 300);
});
`}
            </Code>
        </div>
    );
}
