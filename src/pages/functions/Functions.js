import React from 'react';
import {Code} from "../../components/Code";

export function Functions() {
    return (
        <div>
            <h1>Funkce</h1>

            <p>Kolekce příkazů, která může být zavolána jednou nebo víckrát. Může mít nějaké vstupy a výstupy.</p>

            <Code>
                {`function doSomething() {
    console.log("I do, what's your problem?");
}`}
            </Code>

            <Code>
                {`function doSomething(suggestion, name) {
    console.log(name);
    console.log(suggestion);
    console.log("I do, what's your problem?");
}
doSomething.length;
`}
            </Code>
        </div>
    );
}
